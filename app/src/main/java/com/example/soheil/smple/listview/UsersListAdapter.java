package com.example.soheil.smple.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.soheil.smple.R;

public class UsersListAdapter extends BaseAdapter {

    Context mContext;
    String names[];

    public UsersListAdapter(Context mContext, String[] names) {
        this.mContext = mContext;
        this.names = names;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.item_users_list, parent, false);
        TextView name = v.findViewById(R.id.name);
        name.setText(names[position]);


        return v;
    }
}
