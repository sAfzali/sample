package com.example.soheil.smple.subavtivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.soheil.smple.R;
import com.example.soheil.smple.utils.BaseActivity;
import com.example.soheil.smple.utils.PublicMethods;

public class StartActivity extends BaseActivity implements View.OnClickListener {
    private TextView resultView;
    private int subRequestCode = 1000;
    private String toastMsg = "Data is Empty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        bind();
    }

    private void bind() {
        resultView = findViewById(R.id.result_view);
        findViewById(R.id.get_data).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(StartActivity.this, SubActivity.class);
        startActivityForResult(intent, subRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == subRequestCode) {
            if (resultCode == Activity.RESULT_OK)
                resultView.setText(data.getStringExtra("user_name"));
            else
                PublicMethods.toast(this, getString(R.string.toast_msg));
        }
    }
}
