package com.example.soheil.smple.subavtivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.soheil.smple.R;
import com.example.soheil.smple.utils.BaseActivity;

public class SubActivity extends BaseActivity implements View.OnClickListener {
    private EditText userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        bind();
    }

    private void bind() {
        userName = findViewById(R.id.user_name);
        findViewById(R.id.set_data).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String userValue = userName.getText().toString().trim();
        Intent intent = new Intent();
        intent.putExtra("user_name", userValue);
        if (userValue.length() > 0) {
            setResult(Activity.RESULT_OK, intent);
        } else
            setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}
