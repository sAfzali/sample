package com.example.soheil.smple.sharedprefce;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.soheil.smple.R;
import com.example.soheil.smple.utils.BaseActivity;
import com.example.soheil.smple.utils.Constant;
import com.example.soheil.smple.utils.PublicMethods;

public class SharedPreferenceActivity extends BaseActivity implements View.OnClickListener {
    private EditText name, mobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);
        bind();
        loadData();

    }

    private void bind() {
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.save_data).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String nameValue = name.getText().toString();
        String mobileValue = mobile.getText().toString();
        PublicMethods.saveData(this, Constant.name, nameValue);
        PublicMethods.saveData(this, Constant.mobile, mobileValue);
/*
PublicMethods.saveData(mContext, "name", nameValue);
PublicMethods.saveData(mContext, "mobile", mobileValue);
*/
/*
PreferenceManager.getDefaultSharedPreferences(mContext).edit()
.putString("name", nameValue).apply();
PreferenceManager.getDefaultSharedPreferences(mContext).edit()
.putString("mobile", mobileValue).apply();
*/
        name.setText("");
        mobile.setText("");
        PublicMethods.toast(mContext, getString(R.string.toast_msg));
    }

    void loadData() {
/*
String defUserValue = PreferenceManager.getDefaultSharedPreferences(mContext).
getString("name", "");
String defMobileValue = PreferenceManager.getDefaultSharedPreferences(mContext).
getString("mobile", "");
*/
/*
String defUserValue = PublicMethods.loadData(mContext, "name", "");
String defMobileValue = PublicMethods.loadData(mContext, "mobile", "");
*/
        String defNameValue = PublicMethods.loadData(this, Constant.name, "");
        String defMobileValue = PublicMethods.loadData(this, Constant.mobile, "");
        if (defNameValue.length() > 0) {
            name.setText(defNameValue);
            if (defMobileValue.length() > 0)
                mobile.setText(defMobileValue);
        }
    }
}
