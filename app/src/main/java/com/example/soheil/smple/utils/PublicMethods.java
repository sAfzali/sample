package com.example.soheil.smple.utils;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class PublicMethods {


    public static void toast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static void saveData(Context mContext, String key, String value) {
        Hawk.put(key, value);
        /* PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply(); */
    }

    public static String loadData(Context mContext, String key, String defValue) {
        return Hawk.get(key, defValue);
        /* return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, defValue); */
    }


}
