package com.example.soheil.smple.foodslistview;

import android.os.Bundle;
import android.widget.ListView;

import com.example.soheil.smple.R;
import com.example.soheil.smple.utils.BaseActivity;
import com.example.soheil.smple.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class FoodListActivity extends BaseActivity {
    ListView foodsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        bind();

    }

    void bind() {
        foodsListView = findViewById(R.id.foods_list_view);
        foodModel();

    }

    void foodModel() {
        FoodModel pizza = new FoodModel();
        pizza.setName(Constant.pizza);
        pizza.setPrice(25000);
        pizza.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        FoodModel fesenjoon = new FoodModel();
        fesenjoon.setName(Constant.fesenjoon);
        fesenjoon.setPrice(40000);
        fesenjoon.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        FoodModel mashroom = new FoodModel();
        mashroom.setName(Constant.mashrume);
        mashroom.setPrice(40000);
        mashroom.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        FoodModel burgure = new FoodModel();
        burgure.setName(Constant.burgure);
        burgure.setPrice(40000);
        burgure.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        FoodModel sandwich = new FoodModel();
        sandwich.setName(Constant.sandwich);
        sandwich.setPrice(40000);
        sandwich.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        List<FoodModel> foods = new ArrayList<>();
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(mashroom);
        foods.add(burgure);
        foods.add(sandwich);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(mashroom);
        foods.add(burgure);
        foods.add(sandwich);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(mashroom);
        foods.add(burgure);
        foods.add(sandwich);

        FoodsAdapter adapter = new FoodsAdapter(mContext, foods);
        foodsListView.setAdapter(adapter);

    }


}
