package com.example.soheil.smple.listview;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.soheil.smple.R;
import com.example.soheil.smple.utils.BaseActivity;
import com.example.soheil.smple.utils.Constant;
import com.example.soheil.smple.utils.PublicMethods;

public class UsersListViewActivity extends BaseActivity {
    ListView usersListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list_view);
        bind();
        UsersListAdapter adapter = new UsersListAdapter(mContext, Constant.users);
        usersListView.setAdapter(adapter);
        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PublicMethods.toast(mContext, Constant.item + position);
            }
        });
    }

    void bind() {
        usersListView = findViewById(R.id.users_list_view);
        String users[] = Constant.users;
    }


}
